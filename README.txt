Principe du projet:

    Ship est un jeu de tir se déroulant dans l'espace.
    Le joueur doit tirer sur des astéroïdes ainsi que des aliens afin de marquer des points.
    Le but est d'obtenir le plus haut score.

Problèmes rencontrés:

    Le seul problème que j'ai rencontré c'est comment créer un décalage avec un timer, la solution la plus simple que j'ai pu
    trouver a été l'utilisation d'une coroutine afin d'introduire un timer. Ce n'est cependant pas la plus optimale car elle
    oblige à utiliser un autre coeur afin de lancer un nouveau thread.

Appareils testés:
    - Ordinateur
    - Web
    - (Mobile : mise en place des fonctionnalitées tactiles mais non testées sur un vrai téléphone)