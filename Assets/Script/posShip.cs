﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Script de gestion de la position du vaisseau */

public class posShip : MonoBehaviour
{

    //Angles min et max de la caméra
    private Vector3 rightTopCameraBorder;
    private Vector3 leftTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;

    void Start()
    {
        // Angles avec conversion monde de la caméra -> monde du pixel 
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
    }
    
    void Update()
    {
        //Calcul de la taille du sprite auquel ce script est attaché
        Vector2 size;
        size.x = GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = GetComponent<SpriteRenderer>().bounds.size.y;

        /*Si la position en Y est inférieur à la limite basse de l'écran
        on le repositionne en bas de l'écran */
        if (transform.position.y < leftBottomCameraBorder.y + (size.y / 2))
            gameObject.transform.position = new Vector3(transform.position.x,leftBottomCameraBorder.y + (size.y / 2), transform.position.z);

        /*Si la position en Y est suppérieur à la limite basse de l'écran
        on le repositionne en haut de l'écran */
        if (transform.position.y > leftTopCameraBorder.y - (size.y / 2))
            gameObject.transform.position = new Vector3(transform.position.x,leftTopCameraBorder.y - (size.y / 2), transform.position.z);

        /*Si la position en X est suppérieur à la limite droite de l'écran
        on le repositionne à droite de l'écran */
        if (transform.position.x > rightTopCameraBorder.x - (size.x / 2))
            gameObject.transform.position = new Vector3(rightTopCameraBorder.x - (size.x / 2),transform.position.y, transform.position.z);

        /*Si la position en X est inférieure à la limite gauche de l'écran
        on le repositionne à gauche de l'écran */
        if (transform.position.x < leftTopCameraBorder.x + (size.x / 2))
            gameObject.transform.position = new Vector3(leftTopCameraBorder.x + (size.x / 2),transform.position.y, transform.position.z);
        
    }
}
