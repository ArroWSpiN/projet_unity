﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Script de gestion du tir */

public class shootAgain : MonoBehaviour
{
    private Vector2 siz;

    private Joybutton joybutton;

    void Start()
    {
       joybutton = FindObjectOfType<Joybutton>();
    }

    void Update () {
        
        siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;

        /* On récupère l'information si le touche espace est appuyée */
        bool sp = Input.GetKeyDown (KeyCode.Space);
        if (sp || (joybutton.pressed && joybutton.shoot)) {
            joybutton.shoot = false;
            /* On détermine la position du tir à partir de la position du vaisseau */
            Vector3 tmpPos = new Vector3 (transform.position.x + siz.x,
                transform.position.y,
                transform.position.z);
            int score = gameState.Instance.getScorePlayer(); // On récupère le score
            /* Si le score est inférieur à 25, le tir est orange */
            if (score < 25){
                GameObject gY = Instantiate (Resources.Load ("shootOrange"), tmpPos, Quaternion.identity) as GameObject;
            }
            /* Sinon, l'arme est amélioré et le tir est jaune */
            else {
                GameObject gY = Instantiate (Resources.Load ("shootJaune"), tmpPos, Quaternion.identity) as GameObject;
            }
            
        }
    }
}
