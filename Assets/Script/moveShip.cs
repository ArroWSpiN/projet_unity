﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Script de gestion du dépacement du vaisseau */

public class moveShip : MonoBehaviour
{

    // 1-Vecteur de vitesse
    public Vector2 speed;

    // 2-Vecteur de mouvement
    private Vector2 movement;

    private Joystick joystick;

    void Start()
    {
       joystick = FindObjectOfType<Joystick>(); // On récupère le joystick
    }

    void Update()
    {
        // 3-Infos clavier
        float inputY = Input.GetAxis("Vertical");
        float inputX = Input.GetAxis("Horizontal");

        // 4-Mouvement
        movement = new Vector2(speed.x * inputX + joystick.Horizontal * speed.x, speed.y * inputY + joystick.Vertical * speed.y);
        GetComponent<Rigidbody2D> ().velocity = new Vector2(speed.x * inputX + joystick.Horizontal * speed.x, speed.y * inputY + joystick.Vertical * speed.y);
    }

}