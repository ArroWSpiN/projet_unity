﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Script d'animation pour faire disparaitre les GameObject */

public class fadeOut : MonoBehaviour
{ 
   void Update () {
        /* 
            On récupère la couleur de l'objet et on baisse son alpha jusqu'à le rendre totalement transparent
            puis on le supprime
        */
        Color cl = GetComponent<SpriteRenderer> ().color;
        cl.a -= 0.1f;
        GetComponent<SpriteRenderer> ().color = cl;
        if (cl.a < 0) {
            Destroy (gameObject);
        }
    }
}
