﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Script de gestion des astéroïdes et des aliens */

public class gameManager : MonoBehaviour
{
    private Vector3 rightTopCameraBorder;
    private Vector3 leftTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;
    private GameObject[] respaws;
    private GameObject[] alien;
    private Vector3 siz_Ast;
    private Vector3 siz_Al;
    private int last;

     void Start()
    {
        // Angles avec conversion monde de la caméra -> monde du pixel 
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
    }

    void Update()
    {
        int i = 30; // Fréquence d'apparition des aliens
        /* On récupère dans des tableaux tous les astéroïdes et les aliens */
        respaws = GameObject.FindGameObjectsWithTag("asteroid");
        alien = GameObject.FindGameObjectsWithTag("alien");
        /* Gestion de l'apparition des astéroïdes */
        if (respaws.Length < 10)
        {
            if (Random.Range(1,100) == 50)
            {
                siz_Ast.x = respaws[0].GetComponent<SpriteRenderer>().bounds.size.x * gameObject.transform.localScale.x;
                siz_Ast.y = respaws[0].GetComponent<SpriteRenderer>().bounds.size.y* gameObject.transform.localScale.y;

                Vector3 tmpPosAst = new Vector3((rightBottomCameraBorder.x + (siz_Ast.x / 2)),
                                    Random.Range (rightBottomCameraBorder.y + (siz_Ast.y / 2), (rightTopCameraBorder.y - (siz_Ast.y / 2))),
                                    transform.position.z);

                GameObject gAst = Instantiate (Resources.Load ("Asteroid"), tmpPosAst, Quaternion.identity) as GameObject;

            }
        }

        /* Gestion de l'apparition des aliens toutes les 30 secondes */
        if ((int)(Time.time) % i == 0 && (int)(Time.time)!= last)
        {
            last = (int)Time.time; // Variable pour qu'un seul alien apparaisse pendant la seconde où s'éxécute la boucle

            siz_Al.x = alien[0].GetComponent<SpriteRenderer>().bounds.size.x * gameObject.transform.localScale.x;
            siz_Al.y = alien[0].GetComponent<SpriteRenderer>().bounds.size.y* gameObject.transform.localScale.y;

            Vector3 tmpPosAl = new Vector3((rightBottomCameraBorder.x + (siz_Al.x / 2)),
                                    Random.Range (rightBottomCameraBorder.y + (siz_Al.y / 2), (rightTopCameraBorder.y - (siz_Al.y / 2))),
                                    transform.position.z);

            GameObject gAl = Instantiate (Resources.Load ("Alien"), tmpPosAl, Quaternion.identity) as GameObject;
            
        }

        
    }
}
