﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Script de gestion du dépacement des tirs */

public class moveShoot : MonoBehaviour
{
    //Angles min et max de la caméra
    private Vector3 rightTopCameraBorder;
    private Vector3 leftTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;
    public Vector2 speed;
    private Vector2 size;
    
    void Start()
    {
        // Angles avec conversion monde de la caméra -> monde du pixel 
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
        
    }

    void Update()
    {
        size.x = GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = GetComponent<SpriteRenderer>().bounds.size.y;

        GetComponent<Rigidbody2D>().velocity = new Vector2(speed.x,0.0f); 

        /* Si le tir sort de l'écran, on le détruit */
        if (transform.position.x > rightBottomCameraBorder.x - (size.x / 2))
        {
            Destroy(gameObject);
        }       
    }

    void OnTriggerEnter2D(Collider2D collider) 
    {
        /* Si le tir rencontre un astéroïde, on incrémente le score */
        if(collider.tag == "asteroid"){
            Destroy(gameObject);
            /* Si le tir est orange, on incrémente de 1 */
            if (gameObject.name == "ShootOrange(Clone)"){
                gameState.Instance.addScorePlayer(1);
            }
            /* Si le tir est jaune, on incrémente de 3 car l'arme est amélioré */
            else if (gameObject.name == "ShootJaune(Clone)"){
                gameState.Instance.addScorePlayer(3);
            }            
        }

        /* Idem si le tir rencontre un alien */
        else if(collider.tag == "alien"){
            Destroy(gameObject);
            /* Si le tir est orange, on incrémente de 5 */
            if (gameObject.name == "ShootOrange(Clone)"){
                gameState.Instance.addScorePlayer(5);
            }
            /* Si le tir est jaune, on incrémente de 10 car l'arme est amélioré */
            else if (gameObject.name == "ShootJaune(Clone)"){
                gameState.Instance.addScorePlayer(10);
            }            
        }
    }
}
