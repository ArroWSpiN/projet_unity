﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/* Script de gestion du dépacement des aliens */
public class moveAlien : MonoBehaviour
{
    //Angles min et max de la caméra
    private Vector3 rightTopCameraBorder;
    private Vector3 leftTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;

    // Vecteur de vitesse
    public Vector2 speed;

    private Vector2 size;

    private AudioSource source;

    void Start()
    {
        // Angles avec conversion monde de la caméra -> monde du pixel 
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));

        /* Récupération du clip audio */
        source = gameObject.GetComponent<AudioSource>();
    }

    void DestroyGameObject()
    {
        Destroy(gameObject);
    }

    void Update()   
    {
        //Calcul de la taille du sprite auquel ce script est attaché
        
        size.x = GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = GetComponent<SpriteRenderer>().bounds.size.y;

        GetComponent<Rigidbody2D>().velocity = new Vector2(speed.x,0.0f);

        /* Si l'alien sort de l'écran, alors on le détruit */
        if (transform.position.x < leftBottomCameraBorder.x - (size.x / 2))
        {
            DestroyGameObject();
        }
    }

    void OnTriggerEnter2D(Collider2D collider) 
    {
        /*
            Si l'alien rentre en colision avec le vaisseau, on enlève des PV 
            On regarde avant si l'alien possède le composant fadeOut afin de savoir
            s'il a été touché par un tir avant de rentrer en colision: si c'est le
            cas cele veut dire qu'il a été touché et qu'il est en train de faire
            son animation pour disparaitre, on enlève donc pas de PV
        */
        if(collider.name == "Ship"){
            StartCoroutine(waiter()); // Coroutine pour faire disparaitre l'alien et lancer le son d'explosion
            if (GameObject.FindGameObjectWithTag("life5") && gameObject.GetComponent<fadeOut>() == null) 
                GameObject.FindGameObjectWithTag("life5").AddComponent<fadeOut>();
            else if (GameObject.FindGameObjectWithTag("life4") && gameObject.GetComponent<fadeOut>() == null)
                GameObject.FindGameObjectWithTag("life4").AddComponent<fadeOut>();
            else if (GameObject.FindGameObjectWithTag("life3") && gameObject.GetComponent<fadeOut>() == null)
                GameObject.FindGameObjectWithTag("life3").AddComponent<fadeOut>();
            else if (GameObject.FindGameObjectWithTag("life2") && gameObject.GetComponent<fadeOut>() == null)
                GameObject.FindGameObjectWithTag("life2").AddComponent<fadeOut>();
            else if (GameObject.FindGameObjectWithTag("life1") && gameObject.GetComponent<fadeOut>() == null){
                GameObject.FindGameObjectWithTag("life1").AddComponent<fadeOut>();
                SceneManager.LoadScene("End"); // Si le joueur n'a plus de PV, on charge l'écran de fin
            }
                
        }
        /* Si l'alien a été touché par un tir */
        if((collider.name == "ShootOrange(Clone)") || (collider.name == "ShootJaune(Clone)") ){
            StartCoroutine(waiter());
        }
    }

    /* Coroutine */
    IEnumerator waiter(){
        source.Play();
        /* On attend 0.1 seconde avant de détruire l'alien pour lui laisser le temps de lancer le son d'explosion */
        yield return new WaitForSecondsRealtime(0.1f); 
        gameObject.AddComponent<fadeOut>();

    }
}
