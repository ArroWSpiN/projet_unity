﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Script de gestion du dépacement du fond */

public class moveBk : MonoBehaviour
{

    private Vector3 siz;

    private Vector3 leftBottomCameraBorder;

    public Vector2 movement;

    public float positionRestartX;


    void Start()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0,0,0));
    }

    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = movement;
        siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
        
        /* Si le fond sort de l'écran, on le remet à droite */
        if (transform.position.x < leftBottomCameraBorder.x - (siz.x / 2))
        {
            transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
        }
    }
}
