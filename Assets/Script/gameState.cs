﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/* Script de gestion du score */

public class gameState : MonoBehaviour
{

    public static gameState Instance;
    private int scorePlayer = 0;
    private int record = 0;
    private bool once = true;

    void Start()
    {
        // PlayerPrefs.SetInt("Score", 0)  Initialisation du record au premier démarage du jeu
        record = PlayerPrefs.GetInt("Score", -1); // On utilise PlayerPrefs afin de stocker le record
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (Instance.gameObject);
        }
        else if (this != Instance) {
            Destroy (this.gameObject);
        }       
    }

    public void addScorePlayer(int toAdd) {
        scorePlayer += toAdd;
    }

    public int getScorePlayer(){
        return scorePlayer;
    }

    public void reinitialisation(){
        scorePlayer = 0;
    }

    void FixedUpdate(){
        /* Si on est dans la scène de jeu */ 
        if(SceneManager.GetActiveScene().name == "Game"){
            /* On change le score à chaque fois que le houeur marque un point */
            GameObject.FindWithTag ("scoreLabel").GetComponent<Text>().text = "" + scorePlayer;
            /* S'il arrive à 25 points, on lance la coroutine pour afficher l'amélioration de l'arme */
            if (scorePlayer == 25 && once){
                /* La variable once sert à ne l'afficher qu'une fois et non à boucler tant que le score est à 25 */
                StartCoroutine(waiter());
            }
        }
        /* Si on est dans la scène de fin */
        else if(SceneManager.GetActiveScene().name == "End"){
            if (scorePlayer > record) PlayerPrefs.SetInt("Score", scorePlayer); // Si le score est supérieur au record, on l'enregistre
            /* On affiche le score et le record */
            GameObject.FindWithTag ("points").GetComponent<Text>().text = "" + scorePlayer;
            GameObject.FindWithTag ("record_points").GetComponent<Text>().text = "" + PlayerPrefs.GetInt("Score", -1);
        }
    }

    /* Coroutine pour afficher l'amélioration pendant 3 secondes */
    IEnumerator waiter(){
        once = false;
        GameObject.FindWithTag ("upgrade_weapons").GetComponent<Text>().text = "Armes améliorées ! 3 points par tir !";
        yield return new WaitForSecondsRealtime(3);
        GameObject.FindWithTag ("upgrade_weapons").GetComponent<Text>().text = "";


    }

}
