﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/* Script de passage à la scène du menu */

public class loadMenu : MonoBehaviour
{
    public void onClick(){
        gameState.Instance.reinitialisation(); // Lorsqu'on reviens au menu, le score est réinitialisé
        SceneManager.LoadScene("Menu");
    }
}