﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/* Script de gestion du bouton de tir */

public class Joybutton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{

    public bool pressed; // Booléen qui indique si le bouton est préssé
    public bool shoot = true; // Boléen qui indique si on est en train de tirer

    /* Quand on appuie sur le bouton, on passe pressed à vrai */
    public void OnPointerDown(PointerEventData eventData)
    {
        pressed = true;
    }

    /* Quand on appuie plus sur le bouton, on passe pressed à faux et shoot à vrai pour pouvoir retirer ensuite */
    public void OnPointerUp(PointerEventData eventData)
    {
        pressed = false;
        shoot = true;
    }

}

